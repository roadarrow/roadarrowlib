# PročitajMe.md
### Raison d'être
U svrhe lakšeg zajedničkog rada na projektu Drumska Strela pokrenut je ovaj repozitorijum. Početak istorije se vodi stanjem na SVNu u trenutku kreacije, sa potencijalno par sync commitova dok se svi ne prebace na novi model rada.

### Kako krenuti
Instalirajte [git](https://git-scm.com/) i [git-lfs](https://git-lfs.github.com/). Linkovi su za Windows.

Klonirajte repozitorijum, da znate kad birate mesto za pokretanje komande da se svuku podaci on će napraviti svoj folder u kome će biti sve.

` $ git clone https://gitlab.com/roadarrow/roadarrowlib.git`

Podesite u komandnoj liniji i sledeće:

` $ git config --global user.name "Ime Prezime" `

` $ git config --global user.email "vaš@e.mail" `

### Kratki pregled komandi
Za upustvo imate git priručnik koji se nalazi na lokaciji **TBD**. Ovo služi podsećanju.

` $ git fetch ` --- Sinhronizovanje lokalne istorije sa istorijom na serveru

` $ git status ` --- Provera stanja vašeg lokalnog rada

` $ git checkout -b ime_lokalne_grane ime_lokalne_ili_globalne_grane ` --- ovime se pravi nova grana na lokalu koja ima istu istoriju kao grana ime_lokalne_ili_globalne_grane

` $ git branch ` --- Ispiše sve lokalne grane na tvom sistemu

` $ git branch -r ` --- Ispiše sve globalne grane na tvom sistemu

` $ git add putanja_do_fajla ` --- Dodavanje novog fajla u commit ili dodavanje izmenjenog fajla u commit

` $ git commit -m "poruka_za_commit" ` --- Kada dodate sve fajlove koje bi da sačuvate na server potrebno je napisati poruku koja sadrži kratko objašnjenje šta se nalazi na tom mestu. Poruka poželjno treba da zadovoljava 4 uslova: kratko, bezlično, razumljivo i tačno. Primera radi "Izmene TSMDa usled revizije" ima sva 4 uslova zadovoljena dok npr "a" prolazi samo 2 od 4 uslova.

` $ git push origin ime_lokalne_grane:ime_globalne_grane" ` --- Osim ako ti nije eksplicitno rečeno da smeš ime_globalne_grane NIKAD ne sme biti master. ime_lokalne_grane je lokalna grana koju želiš da postaviš na server. ime_globalne_grane je ime koje daješ svojoj grani na serveru, ne mora biti isto, prihvata se grupisanje u foldere, npr `$ git push origin revizija_BMS:svukcevic/revizija_BMS`. Takođe imate pravo da postavite nove komitove na postojeću granu, sve dok ne menjate prethodnu istoriju, npr `$ git push origin revizija_BMS:nneskovic/BMS_razvoj`

` $ git checkout putanja_do_fajla` --- Resetuje fajl na stanje pri poslednjem commitu. Ako ste se zeznuli. JBG.

### PRAVILA
1. **NIKAD PUSH NA MASTER**, to je posao integratora posle provere vaše grane